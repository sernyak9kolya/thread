import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, status, imageId, image } = await userRepository.getUserById(userId);
  return { id, username, email, status, imageId, image };
};

export const updateUserById = async (userId, data) => {
  if (data.username) {
    const user = await userRepository.getByUsername(data.username);

    if (user) {
      throw Error('The user with such name is already exist');
    }
  }

  await userRepository.updateById(userId, data);
  const { id, username, email, status, imageId, image } = await getUserById(userId);
  return { id, username, email, status, imageId, image };
};
