import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';
import userRepository from '../../data/repositories/userRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const getReaction = async (userId, commentId) => (
  await commentReactionRepository.getCommentReaction(userId, commentId) || {}
);

export const getLikers = async postId => userRepository.getCommentReacters(postId, true);

export const getDislikers = async postId => userRepository.getCommentReacters(postId, false);

export const updateCommentById = async (id, data) => {
  await commentRepository.updateById(id, data);
  return getCommentById(id);
};

export const setReaction = async (userId, commentId, { isLike = true }) => {
  const updateOrDelete = react => (react.isLike === isLike
    ? commentReactionRepository.deleteById(react.id)
    : commentReactionRepository.updateById(react.id, { isLike }));

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  return Number.isInteger(result) ? {} : commentReactionRepository.getCommentReaction(userId, commentId);
};

export const deleteCommentById = async id => {
  const deleted = Boolean(await commentRepository.deleteById(id));
  return { deleted };
};
