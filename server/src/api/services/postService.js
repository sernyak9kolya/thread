import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import userRepository from '../../data/repositories/userRepository';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const getReaction = async (userId, postId) => (
  await postReactionRepository.getPostReaction(userId, postId) || {}
);

export const getLikers = async postId => userRepository.getPostReacters(postId, true);

export const getDislikers = async postId => userRepository.getPostReacters(postId, false);

export const setReaction = async (userId, postId, { isLike = true }) => {
  // define the callback for future use as a promise
  const updateOrDelete = react => (react.isLike === isLike
    ? postReactionRepository.deleteById(react.id)
    : postReactionRepository.updateById(react.id, { isLike }));

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  return Number.isInteger(result) ? {} : postReactionRepository.getPostReaction(userId, postId);
};

export const update = async (userId, postId, body = '') => {
  const { user } = await postRepository.getPostById(postId);

  if (userId !== user.id) {
    throw Error('You cannot update someone else\'s post!');
  }

  return postRepository.updateById(postId, body);
};

export const destroy = async (userId, postId) => {
  const post = await postRepository.getPostById(postId);

  if (!post) {
    throw Error('This post is already deleted!');
  } else if (userId !== post.user.id) {
    throw Error('You cannot delete someone else\'s post!');
  }

  const deleted = Boolean(await postRepository.deleteById(postId));
  return { deleted };
};
