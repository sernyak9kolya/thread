import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .get('/:id/react', (req, res, next) => commentService.getReaction(req.user.id, req.params.id)
    .then(reaction => res.send(reaction))
    .catch(next))
  .get('/:id/likers', (req, res, next) => commentService.getLikers(req.params.id)
    .then(likers => res.send(likers))
    .catch(next))
  .get('/:id/dislikers', (req, res, next) => commentService.getDislikers(req.params.id)
    .then(likers => res.send(likers))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateCommentById(req.params.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/:id/react', (req, res, next) => commentService.setReaction(req.user.id, req.params.id, req.body)
    .then(reaction => res.send(reaction))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteCommentById(req.params.id)
    .then(message => res.send(message))
    .catch(next));

export default router;
