import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .get('/:id/react', (req, res, next) => postService.getReaction(req.user.id, req.params.id)
    .then(reaction => res.send(reaction))
    .catch(next))
  .get('/:id/likers', (req, res, next) => postService.getLikers(req.params.id)
    .then(likers => res.send(likers))
    .catch(next))
  .get('/:id/dislikers', (req, res, next) => postService.getDislikers(req.params.id)
    .then(likers => res.send(likers))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.update(req.user.id, req.params.id, req.body)
    .then(post => res.send(post))
    .catch(next))
  .put('/:id/react', (req, res, next) => postService.setReaction(req.user.id, req.params.id, req.body)
    .then(async reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        const { isLike } = await postService.getReaction(req.user.id, req.params.id);
        if (isLike) {
          req.io.to(reaction.post.userId).emit('like', 'Your post was liked!');
        } else {
          req.io.to(reaction.post.userId).emit('dislike', 'Your post was disliked!');
        }
      }
      return res.send(reaction);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => postService.destroy(req.user.id, req.params.id)
    .then(post => res.send(post))
    .catch(next));

export default router;
