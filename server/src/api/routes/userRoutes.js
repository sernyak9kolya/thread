import { Router } from 'express';
import * as userService from '../services/userService';

const router = new Router();

router
  .put('/', (req, res, next) => userService.updateUserById(req.user.id, req.body)
    .then(user => res.send(user))
    .catch(error => {
      req.io.to(req.user.id).emit('error', error.message);
      next(error);
    }));

export default router;
