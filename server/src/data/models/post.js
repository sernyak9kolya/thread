export default (orm, DataTypes) => {
  const Post = orm.define('post', {
    body: {
      allowNull: false,
      type: DataTypes.TEXT
    },
    createdAt: DataTypes.DATE,
    updatedAt: DataTypes.DATE,
    deletedAt: {
      allowNull: true,
      type: DataTypes.DATE
    }
  }, {
    paranoid: true
  });

  return Post;
};
