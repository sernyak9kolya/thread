import Sequelize from 'sequelize';
import sequelize from '../db/connection';
import { PostModel, CommentModel, UserModel, ImageModel, PostReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

const selectCommentCount = () => `(
  SELECT COUNT(*)
  FROM "comments" as "comment"
  WHERE "post"."id" = "comment"."postId" AND "comment"."deletedAt" ISNULL)`;

const selectPostReactionCount = bool => `(
  SELECT COUNT(*)
  FROM "postReactions" as "postReaction"
  WHERE "postReaction"."postId" = "post"."id" AND "postReaction"."isLike" = ${bool})`;

const selectCommentReactionCount = bool => `(
  SELECT COUNT(*)
  FROM "commentReactions" as "commentReaction"
  WHERE "commentReaction"."commentId" = "comments"."id" AND "commentReaction"."isLike" = ${bool})`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      userId,
      likedBy
    } = filter;

    const where = {};
    const whereReactions = {};

    if (userId) {
      const [id, operator = 'eq'] = userId.split(':').reverse();
      Object.assign(where, {
        userId: {
          [Sequelize.Op[operator]]: id
        }
      });
    }

    if (likedBy) {
      Object.assign(whereReactions, {
        userId: likedBy,
        isLike: true
      });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(selectCommentCount()), 'commentCount'],
          [sequelize.literal(selectPostReactionCount(true)), 'likeCount'],
          [sequelize.literal(selectPostReactionCount(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: PostReactionModel,
        where: whereReactions,
        attributes: [],
        duplicating: false
      }],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'user.id',
        'user->image.id',
        'image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(selectCommentCount()), 'commentCount'],
          [sequelize.literal(selectPostReactionCount(true)), 'likeCount'],
          [sequelize.literal(selectPostReactionCount(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: CommentModel,
        attributes: {
          include: [
            [sequelize.literal(selectCommentReactionCount(true)), 'likeCount'],
            [sequelize.literal(selectCommentReactionCount(false)), 'dislikeCount']
          ]
        },
        include: [
          {
            model: UserModel,
            attributes: ['id', 'username', 'status'],
            include: {
              model: ImageModel,
              attributes: ['id', 'link']
            }
          }
        ]
      }, {
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }, {
        model: ImageModel,
        attributes: ['id', 'link']
      }, {
        model: PostReactionModel,
        attributes: []
      }]
    });
  }
}

export default new PostRepository(PostModel);
