import { UserModel, ImageModel, PostReactionModel, CommentReactionModel } from '../models/index';
import BaseRepository from './baseRepository';

class UserRepository extends BaseRepository {
  addUser(user) {
    return this.create(user);
  }

  getByEmail(email) {
    return this.model.findOne({ where: { email } });
  }

  getByUsername(username) {
    return this.model.findOne({ where: { username } });
  }

  getUserById(id) {
    return this.model.findOne({
      group: [
        'user.id',
        'image.id'
      ],
      where: { id },
      include: {
        model: ImageModel,
        attributes: ['id', 'link']
      }
    });
  }

  getPostReacters(postId, isLike) {
    return this.model.findAll({
      attributes: ['id', 'username'],
      include: {
        model: PostReactionModel,
        where: {
          postId,
          isLike
        },
        attributes: []
      }
    });
  }

  getCommentReacters(commentId, isLike) {
    return this.model.findAll({
      attributes: ['id', 'username'],
      include: {
        model: CommentReactionModel,
        where: {
          commentId,
          isLike
        },
        attributes: []
      }
    });
  }
}

export default new UserRepository(UserModel);
