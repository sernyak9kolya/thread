import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';
import UpdateComment from 'src/components/UpdateComment';

import styles from './styles.module.scss';

const Comment = ({
  userId,
  comment: { id, body, createdAt, user, likeCount, dislikeCount, likers, dislikers },
  update,
  toggleDelete,
  reactComment,
  getReacters
}) => {
  const [editable, setEditable] = useState(false);

  const likeComment = () => reactComment(id, { isLike: true });
  const dislikeComment = () => reactComment(id, { isLike: false });

  const getLikers = () => {
    if (!likers) {
      getReacters(id, { isLike: true });
    }
  };
  const getDislikers = () => {
    if (!dislikers) {
      getReacters(id, { isLike: false });
    }
  };

  const likeLabel = (
    <Label basic size="tiny" as="a" className={styles.toolbarBtn} onClick={likeComment}>
      <Icon name="thumbs up" />
      { likeCount }
    </Label>
  );
  const dislikeLabel = (
    <Label basic size="tiny" as="a" className={styles.toolbarBtn} onClick={dislikeComment}>
      <Icon name="thumbs down" />
      { dislikeCount }
    </Label>
  );

  const commentContent = editable
    ? <UpdateComment comment={{ id, body }} update={update} close={() => setEditable(false)} />
    : <CommentUI.Text>{body}</CommentUI.Text>;

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>{moment(createdAt).fromNow()}</CommentUI.Metadata>
        <br />
        { user.status && <CommentUI.Metadata style={{ margin: '0' }}>{user.status}</CommentUI.Metadata> }
        <CommentUI.Text>{commentContent}</CommentUI.Text>
        <div className={styles.commentFooter}>
          <div>
            <Popup trigger={likeLabel} inverted onOpen={getLikers} popperDependencies={[!!likers]}>
              {likers ? likers.map(u => <p key={u.id}>{u.username}</p>) : 'Loading...'}
            </Popup>
            <Popup trigger={dislikeLabel} inverted onOpen={getDislikers} popperDependencies={[!!dislikers]}>
              {dislikers ? dislikers.map(u => <p key={u.id}>{u.username}</p>) : 'Loading...'}
            </Popup>
          </div>
          {userId === user.id && !editable && (
            <CommentUI.Actions>
              <CommentUI.Action onClick={() => setEditable(true)}>Edit</CommentUI.Action>
              <CommentUI.Action onClick={toggleDelete}>Delete</CommentUI.Action>
            </CommentUI.Actions>
          )}
        </div>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  userId: PropTypes.string,
  update: PropTypes.func.isRequired,
  toggleDelete: PropTypes.func,
  getReacters: PropTypes.func.isRequired,
  reactComment: PropTypes.func.isRequired
};

Comment.defaultProps = {
  userId: undefined,
  toggleDelete: () => {}
};

export default Comment;
