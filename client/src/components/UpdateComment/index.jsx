import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UpdateComment = ({ comment, update, close }) => {
  const [body, setBody] = useState(comment.body);
  const [isLoading, setIsLoading] = useState(false);

  const handleUpdateComment = async () => {
    if (!body) {
      return;
    }
    setIsLoading(true);

    try {
      await update(comment.id, { body });
    } finally {
      setIsLoading(false);
      close();
    }
  };

  return (
    <Form onSubmit={handleUpdateComment}>
      <Form.TextArea value={body} placeholder="Type a comment..." onChange={ev => setBody(ev.target.value)} />
      <div className={styles.actions}>
        <Button basic size="small" onClick={close}>Cancel</Button>
        <Button size="small" type="submit" color="blue" loading={isLoading}>Update</Button>
      </div>
    </Form>
  );
};

UpdateComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  update: PropTypes.func,
  close: PropTypes.func
};

UpdateComment.defaultProps = {
  update: () => {},
  close: () => {}
};

export default UpdateComment;
