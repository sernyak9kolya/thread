import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'semantic-ui-react';

const DeletePost = ({ id, destroy, close }) => {
  const [isLoading, setIsLoading] = useState(false);

  const deletePost = async () => {
    setIsLoading(true);
    try {
      await destroy(id);
    } finally {
      // TODO catch error
      setIsLoading(true);
      close();
    }
  };

  return (
    <Modal open size="tiny" closeOnDimmerClick={false}>
      <Modal.Header>Delete Post</Modal.Header>
      <Modal.Content>Are you sure you wanna delete this post?</Modal.Content>
      <Modal.Actions>
        <Button basic onClick={close}>Cansel</Button>
        <Button color="blue" loading={isLoading} onClick={deletePost}>Delete</Button>
      </Modal.Actions>
    </Modal>
  );
};

DeletePost.propTypes = {
  id: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
  destroy: PropTypes.func.isRequired
};

export default DeletePost;
