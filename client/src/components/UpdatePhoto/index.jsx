import React, { useState, useEffect, useCallback, useRef } from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import { Segment, Modal, Button, Icon, Header } from 'semantic-ui-react';

import 'react-image-crop/dist/ReactCrop.css';

function getResizedCanvas(canvas, newWidth, newHeight) {
  const tmpCanvas = document.createElement('canvas');
  tmpCanvas.width = newWidth;
  tmpCanvas.height = newHeight;

  const ctx = tmpCanvas.getContext('2d');
  ctx.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, newWidth, newHeight);

  return tmpCanvas;
}

const UpdatePhoto = ({ update, uploadImage, close }) => {
  const [photoCrop, setPhotoCrop] = useState({ aspect: 1 / 1 });
  const [completedCrop, setCompletedCrop] = useState(null);
  const [uploadedPhoto, setUploadedPhto] = useState(null);
  const [loading, setLoading] = useState(false);
  const previewCanvasRef = useRef(null);
  const imgRef = useRef(null);

  const handleUploadFile = ({ target }) => {
    if (target.files && target.files.length > 0) {
      const reader = new FileReader();
      reader.addEventListener('load', () => setUploadedPhto(reader.result));
      reader.readAsDataURL(target.files[0]);
    }
  };

  const onLoad = useCallback(img => {
    imgRef.current = img;
  }, []);

  useEffect(() => {
    if (!completedCrop || !previewCanvasRef.current || !imgRef.current) {
      return;
    }

    const image = imgRef.current;
    const canvas = previewCanvasRef.current;
    const crop = completedCrop;
    const dpr = window.devicePixelRatio || 1;

    const scaleX = image.naturalWidth / image.width;
    const scaleY = image.naturalHeight / image.height;
    const ctx = canvas.getContext('2d');

    canvas.width = crop.width * dpr;
    canvas.height = crop.height * dpr;

    ctx.drawImage(
      image,
      crop.x * scaleX,
      crop.y * scaleY,
      crop.width * scaleX,
      crop.height * scaleY,
      0,
      0,
      crop.width * dpr,
      crop.height * dpr
    );
  }, [completedCrop]);

  const handleUpdatePhoto = (previewCanvas, crop) => {
    if (!crop || !previewCanvas) {
      return;
    }

    const dpr = window.devicePixelRatio || 1;
    const canvas = dpr !== 1 ? getResizedCanvas(previewCanvas, crop.width, crop.height) : previewCanvas;

    canvas.toBlob(async blob => {
      setLoading(true);
      try {
        const { id: imageId } = await uploadImage(blob);
        await update({ imageId });
      } finally {
        setLoading(false);
        close();
      }
    });
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header>Update your photo</Modal.Header>
      <Modal.Content>
        <Segment.Group>
          <Segment textAlign="center">
            <Header as="h4">Uploaded photo</Header>
            <ReactCrop
              src={uploadedPhoto}
              crop={photoCrop}
              maxHeight="200"
              maxWidth="200"
              onImageLoaded={onLoad}
              onChange={c => setPhotoCrop(c)}
              onComplete={c => setCompletedCrop(c)}
            />
          </Segment>
          <Segment textAlign="center">
            <Header as="h4">Preview</Header>
            <canvas
              ref={previewCanvasRef}
              style={{
                width: completedCrop?.width ?? 0,
                height: completedCrop?.height ?? 0
              }}
            />
          </Segment>
        </Segment.Group>
      </Modal.Content>
      <Modal.Actions>
        <Button
          icon
          as="label"
          color="teal"
          labelPosition="left"
        >
          <Icon name="image" />
          Select image
          <input name="image" type="file" hidden onChange={handleUploadFile} />
        </Button>
        <Button
          color="blue"
          content="Update"
          disabled={!completedCrop?.width || !completedCrop?.height}
          loading={loading}
          onClick={() => handleUpdatePhoto(previewCanvasRef.current, completedCrop)}
        />
      </Modal.Actions>
    </Modal>
  );
};

UpdatePhoto.propTypes = {
  close: PropTypes.func.isRequired,
  update: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired
};

export default UpdatePhoto;
