import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Input, Icon, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateStatus }) => {
  const [status, setStatus] = useState(user.status);
  const [editableStatus, setEditableStatus] = useState(false);
  const [loading, setLoading] = useState(false);

  const handleUpdateStatus = async () => {
    setLoading(true);
    try {
      await updateStatus({ status });
    } finally {
      setLoading(false);
      setEditableStatus(false);
    }
  };

  const statusInput = (
    <div>
      <Input value={status} size="mini" onInput={e => setStatus(e.target.value)} />
      {' '}
      <Button basic icon="cancel" size="mini" onClick={() => setEditableStatus(false)} />
      <Button basic icon="checkmark" size="mini" loading={loading} onClick={handleUpdateStatus} />
    </div>
  );

  const statusHeader = (
    <div>
      <HeaderUI as="span" sub>{user.status}</HeaderUI>
      {' '}
      <Button basic icon="pencil alternate" size="mini" onClick={() => setEditableStatus(true)} />
    </div>
  );

  const statusElement = (
    user.status && (
      editableStatus
        ? statusInput : statusHeader
    )
  );

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <NavLink exact to="/">
                <Image circular size="mini" src={getUserImgLink(user.image)} />
              </NavLink>
              <div style={{ marginLeft: '10px' }}>
                <NavLink exact to="/">
                  <HeaderUI>{user.username}</HeaderUI>
                </NavLink>
                { statusElement }
              </div>
            </div>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateStatus: PropTypes.func
};

Header.defaultProps = {
  updateStatus: () => {}
};

export default Header;
