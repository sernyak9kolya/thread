import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';

import styles from './styles.module.scss';

const Post = ({
  userId, post, reactPost, toggleExpandedPost, sharePost, toggleUpdatedPost, toggleDeletedPost, getReacters }) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt,
    likers,
    dislikers
  } = post;
  const date = moment(createdAt).fromNow();

  const likePost = () => reactPost(id, { isLike: true });
  const dislikePost = () => reactPost(id, { isLike: false });

  const getLikers = () => {
    if (!likers) {
      getReacters(id, { isLike: true });
    }
  };
  const getDislikers = () => {
    if (!dislikers) {
      getReacters(id, { isLike: false });
    }
  };

  const likeLabel = (
    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={likePost}>
      <Icon name="thumbs up" />
      {likeCount}
    </Label>
  );
  const dislikeLabel = (
    <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={dislikePost}>
      <Icon name="thumbs down" />
      {dislikeCount}
    </Label>
  );

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta className={styles.postHeader}>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          { user.id === userId && (
            <div>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleUpdatedPost(id)}>
                <Icon name="edit" />
              </Label>
              <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleDeletedPost(id)}>
                <Icon name="trash" />
              </Label>
            </div>
          )}
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup trigger={likeLabel} inverted onOpen={getLikers} popperDependencies={[!!likers]}>
          {likers ? likers.map(u => <p key={u.id}>{u.username}</p>) : 'Loading...'}
        </Popup>
        <Popup trigger={dislikeLabel} inverted onOpen={getDislikers} popperDependencies={[!!dislikers]}>
          {dislikers ? dislikers.map(u => <p key={u.id}>{u.username}</p>) : 'Loading...'}
        </Popup>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  userId: PropTypes.string,
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  reactPost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleUpdatedPost: PropTypes.func,
  toggleDeletedPost: PropTypes.func,
  getReacters: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

Post.defaultProps = {
  userId: undefined,
  toggleUpdatedPost: () => {},
  toggleDeletedPost: () => {}
};

export default Post;
