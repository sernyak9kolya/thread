import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'semantic-ui-react';

const DeleteComment = ({ id, destroy, close }) => {
  const [isLoading, setIsLoading] = useState(false);

  const handleDeleteComment = async () => {
    setIsLoading(true);

    try {
      await destroy(id);
    } finally {
      setIsLoading(false);
      close();
    }
  };

  return (
    <Modal open size="tiny">
      <Modal.Header>Delete Comment</Modal.Header>
      <Modal.Content>Do you really wanna delete this comment?</Modal.Content>
      <Modal.Actions>
        <Button basic onClick={close}>Cansel</Button>
        <Button color="blue" loading={isLoading} onClick={handleDeleteComment}>Delete</Button>
      </Modal.Actions>
    </Modal>
  );
};

DeleteComment.propTypes = {
  id: PropTypes.string.isRequired,
  destroy: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default DeleteComment;
