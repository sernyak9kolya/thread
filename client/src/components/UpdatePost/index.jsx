import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Form, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UpdatedPost = ({ post, updatePost, close }) => {
  const [body, setBody] = useState(post.body);
  const [isLoading, setIsLoading] = useState(false);

  const handleUpdatePost = async () => {
    setIsLoading(true);

    try {
      await updatePost(post.id, { body });
    } finally {
      // TODO: show error
      setIsLoading(false);
      close();
    }
  };

  return (
    <Modal size="small" open onClose={close}>
      <Modal.Header>Update Post</Modal.Header>
      <Modal.Content>
        <Form onSubmit={handleUpdatePost}>
          <Form.TextArea
            rows={4}
            placeholder="What is the news?"
            value={body}
            onChange={ev => setBody(ev.target.value)}
          />
          <div className={styles.formActions}>
            <Button color="blue" type="submit" loading={isLoading} disabled={!body}>Update</Button>
          </div>
        </Form>
      </Modal.Content>
    </Modal>
  );
};

UpdatedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  updatePost: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired
};

export default UpdatedPost;
