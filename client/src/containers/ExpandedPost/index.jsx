import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import DeleteComment from 'src/components/DeleteComment';
import Spinner from 'src/components/Spinner';
import {
  reactPost,
  toggleExpandedPost,
  addComment,
  reactComment,
  updateComment,
  deleteComment,
  getPostReacters,
  getCommentReacters
} from 'src/containers/Thread/actions';

const ExpandedPost = ({
  userId,
  post,
  sharePost,
  reactPost: react,
  toggleExpandedPost: toggle,
  addComment: add,
  updateComment: update,
  reactComment: reactionComment,
  deleteComment: destroy,
  getPostReacters: getPReacters,
  getCommentReacters: getCReacters
}) => {
  const [deletedComment, setDeletedComment] = useState(undefined);

  return (
    <Modal open onClose={() => toggle()}>
      {post
        ? (
          <Modal.Content>
            <Post
              post={post}
              reactPost={react}
              toggleExpandedPost={toggle}
              sharePost={sharePost}
              getReacters={getPReacters}
            />
            <CommentUI.Group style={{ maxWidth: '100%' }}>
              <Header as="h3" dividing>
                Comments
              </Header>
              {post.comments && post.comments
                .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
                .map(comment => (
                  <Comment
                    userId={userId}
                    key={comment.id}
                    comment={comment}
                    update={update}
                    toggleDelete={() => setDeletedComment(comment.id)}
                    reactComment={reactionComment}
                    getReacters={getCReacters}
                  />
                ))}
              <AddComment postId={post.id} addComment={add} />
            </CommentUI.Group>
            { deletedComment && (
              <DeleteComment
                id={deletedComment}
                close={() => setDeletedComment(undefined)}
                destroy={destroy}
              />
            )}
          </Modal.Content>
        )
        : <Spinner />}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  reactPost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  reactComment: PropTypes.func.isRequired,
  updateComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  userId: PropTypes.string,
  getPostReacters: PropTypes.func.isRequired,
  getCommentReacters: PropTypes.func.isRequired
};

ExpandedPost.defaultProps = {
  userId: undefined
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost,
  userId: rootState.profile.user.id
});

const actions = {
  reactPost,
  toggleExpandedPost,
  addComment,
  reactComment,
  updateComment,
  deleteComment,
  getPostReacters,
  getCommentReacters
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
