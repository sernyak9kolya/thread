import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const updatePostAction = post => ({
  type: UPDATE_POST,
  post
});

const deletePostAction = id => ({
  type: DELETE_POST,
  id
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const updatePost = (id, body) => async dispatch => {
  await postService.updatePost(id, body);
  const updatedPost = await postService.getPost(id);
  dispatch(updatePostAction(updatedPost));
};

export const deletePost = id => async dispatch => {
  const { deleted } = await postService.deletePost(id);
  if (deleted) {
    dispatch(deletePostAction(id));
  }
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const reactPost = (postId, reaction) => async (dispatch, getRootState) => {
  const { isLike: wasLike } = await postService.getPostReaction(postId);
  const { isLike } = await postService.reactPost(postId, reaction);

  // eslint-disable-next-line no-nested-ternary
  const likeDiff = isLike ? 1 : wasLike ? -1 : 0;
  // eslint-disable-next-line no-nested-ternary
  const dislikeDiff = isLike === false ? 1 : wasLike === false ? -1 : 0;

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + likeDiff,
    dislikeCount: Number(post.dislikeCount) + dislikeDiff
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const getPostReacters = (postId, { isLike }) => async (dispatch, getRootState) => {
  const reacters = {};
  if (isLike) {
    reacters.likers = await postService.getPostLikers(postId);
  } else {
    reacters.dislikers = await postService.getPostDislikers(postId);
  }

  const { posts: { posts, expandedPost } } = getRootState();

  const mapReacters = post => ({
    ...post,
    ...reacters
  });

  const updated = posts.map(post => (post.id !== postId ? post : mapReacters(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapReacters(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const reactComment = (commentId, reaction) => async (dispatch, getRootState) => {
  const { isLike: wasLike } = await commentService.getCommentReaction(commentId);
  const { isLike } = await commentService.reactComment(commentId, reaction);

  // eslint-disable-next-line no-nested-ternary
  const likeDiff = isLike ? 1 : wasLike ? -1 : 0;
  // eslint-disable-next-line no-nested-ternary
  const dislikeDiff = isLike === false ? 1 : wasLike === false ? -1 : 0;

  const mapLikes = comment => ({
    ...comment,
    likeCount: Number(comment.likeCount) + likeDiff,
    dislikeCount: Number(comment.dislikeCount) + dislikeDiff
  });

  const mapCommentLikes = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id === commentId
      ? mapLikes(comment)
      : comment))
  });

  const { posts: { expandedPost } } = getRootState();

  if (expandedPost) {
    dispatch(setExpandedPostAction(mapCommentLikes(expandedPost)));
  }
};

export const getCommentReacters = (commentId, { isLike }) => async (dispatch, getRootState) => {
  const reacters = {};
  if (isLike) {
    reacters.likers = await commentService.getCommentLikers(commentId);
  } else {
    reacters.dislikers = await commentService.getCommentDislikers(commentId);
  }

  const { posts: { expandedPost } } = getRootState();

  const mapReacters = post => ({
    ...post,
    ...reacters
  });

  const mapCommentReacters = post => ({
    ...post,
    comments: post.comments.map(comment => (comment.id === commentId
      ? mapReacters(comment)
      : comment))
  });

  if (expandedPost) {
    dispatch(setExpandedPostAction(mapCommentReacters(expandedPost)));
  }
};

export const updateComment = (id, data) => async (dispatch, getRootState) => {
  const comment = await commentService.updateComment(id, data);
  const { posts: { expandedPost } } = getRootState();

  const mapComments = post => {
    const index = post.comments.findIndex(c => c.id === id);
    return {
      ...post,
      comments: [
        ...post.comments.slice(0, index),
        comment,
        ...post.comments.slice(index + 1)
      ]
    };
  };

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const deleteComment = id => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(id);
  await commentService.deleteComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [
      ...post.comments.filter(c => c.id !== id)
    ]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
