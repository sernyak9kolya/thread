import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  UPDATE_POST,
  DELETE_POST,
  SET_EXPANDED_POST
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case UPDATE_POST: {
      const index = state.posts.findIndex(p => p.id === action.post.id);

      return {
        ...state,
        posts: [
          ...state.posts.slice(0, index),
          action.post,
          ...state.posts.slice(index + 1)
        ]
      };
    }
    case DELETE_POST:
      return {
        ...state,
        posts: [
          ...state.posts.filter(p => p.id !== action.id)
        ]
      };
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    default:
      return state;
  }
};
