import React, { useState } from 'react';
import { bindActionCreators } from 'redux';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as imageService from 'src/services/imageService';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Grid, Segment, Image, Input, Button } from 'semantic-ui-react';
import UpdatePhoto from '../../components/UpdatePhoto';
import { updateUser } from './actions';

const Profile = ({ user, updateUser: update }) => {
  const [username, setUsername] = useState(user.username);
  const [usernameLoading, setUsernameLoading] = useState(false);

  const [status, setStatus] = useState(user.status);
  const [statusLoading, setStatusLoading] = useState(false);

  const [photoModal, setPhotoModal] = useState(false);

  const handleUsernameSubmit = async () => {
    setUsernameLoading(true);
    try {
      await update({ username });
    } finally {
      setUsernameLoading(false);
    }
  };

  const handleStatusSubmit = async () => {
    setStatusLoading(true);
    try {
      await update({ status });
    } finally {
      setStatusLoading(false);
    }
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Segment basic textAlign="center">
          <Image centered src={getUserImgLink(user.image)} size="medium" circular />
          <Button
            content="Update photo"
            icon="image"
            labelPosition="left"
            style={{ marginTop: '12px' }}
            onClick={() => setPhotoModal(true)}
          />
        </Segment>
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          value={username}
          onInput={e => setUsername(e.target.value)}
        />
        {' '}
        <Button
          color="blue"
          icon="save"
          size="small"
          disabled={username === user.username}
          loading={usernameLoading}
          onClick={handleUsernameSubmit}
        />
        <br />
        <br />
        <Input
          icon="bullhorn"
          iconPosition="left"
          placeholder="Here could be your status..."
          type="text"
          value={status}
          onInput={e => setStatus(e.target.value)}
        />
        {' '}
        <Button
          color="blue"
          icon="save"
          size="small"
          disabled={status === user.status}
          loading={statusLoading}
          onClick={handleStatusSubmit}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        { photoModal && <UpdatePhoto close={() => setPhotoModal(false)} update={update} uploadImage={uploadImage} /> }
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateUser: PropTypes.func
};

Profile.defaultProps = {
  user: {},
  updateUser: () => {}
};

const actions = {
  updateUser
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
