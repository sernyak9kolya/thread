import callWebApi from 'src/helpers/webApiHelper';

export const addComment = async request => {
  const response = await callWebApi({
    endpoint: '/api/comments',
    type: 'POST',
    request
  });
  return response.json();
};

export const getComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const updateComment = async (id, data) => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'PUT',
    request: data
  });
  return response.json();
};

export const deleteComment = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}`,
    type: 'DELETE'
  });
  return response.json();
};

export const getCommentReaction = async id => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}/react`,
    type: 'GET'
  });
  return response.json();
};

export const reactComment = async (id, reaction) => {
  const response = await callWebApi({
    endpoint: `/api/comments/${id}/react`,
    type: 'PUT',
    request: reaction
  });
  return response.json();
};

export const getCommentLikers = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}/likers/`,
    type: 'GET'
  });
  return response.json();
};

export const getCommentDislikers = async commentId => {
  const response = await callWebApi({
    endpoint: `/api/comments/${commentId}/dislikers/`,
    type: 'GET'
  });
  return response.json();
};
