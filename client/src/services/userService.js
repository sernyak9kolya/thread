import callWebApi from 'src/helpers/webApiHelper';

export const updateUser = async request => {
  const response = await callWebApi({
    endpoint: '/api/users',
    type: 'PUT',
    request
  });
  return response.json();
};
